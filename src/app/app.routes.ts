import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './module/people';
const ROUTINGS: Routes = [
    {
        path:'',
        redirectTo:'peoples',
        pathMatch:'full'
    },
    {
        path:'peoples',
        component:MainComponent
    }
]

export const RootRouting = RouterModule.forRoot(ROUTINGS);