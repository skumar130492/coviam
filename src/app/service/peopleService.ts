import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PeoplesServices {

    peoplesUrl: string = 'https://swapi.co/api/people/';

    constructor(private http: Http) { }

    getPeoplesData() {
        return this.http.get(this.peoplesUrl).map(res => res.json());
    }
    getFilmsData(url) {
        return this.http.get(url).map(res => res.json());
    }
   
}
