import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MainComponent } from './module/people';
import { RootRouting } from './app.routes';
import { PeoplesServices } from './service/peopleService';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RootRouting,
    Ng2SearchPipeModule
  ],
  providers: [ PeoplesServices ],
  bootstrap: [AppComponent]
})
export class AppModule { }
