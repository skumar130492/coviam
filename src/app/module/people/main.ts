import { Component } from '@angular/core';
import { PeoplesServices } from '../../service/peopleService';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-main',
    templateUrl: 'main.html',
    styleUrls: ['main.css']
})
export class MainComponent {
    public peopleApiRes = []

    constructor(private peoplesServices: PeoplesServices) {
        this.peoplesServices.getPeoplesData().subscribe(res => {
            if (res) {
                this.peopleApiRes = res.results;
                let peoples = [];
                for (let i = 0; i < res.results.length; i++) {
                    let filmTitles = [];
                    if (res.results[i].films.length > 0) {
                        for (let j = 0; j < res.results[i].films.length; j++) {
                            if (res.results[i].films[j]) {
                                this.peoplesServices.getFilmsData(res.results[i].films[j]).subscribe(res => {
                                    filmTitles.push(res.title);
                                });
                            }
                        }

                        this.peopleApiRes[i].filmTitles = filmTitles;
                    }
                }
            }
        });
    }



}